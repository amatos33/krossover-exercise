<?php

use Illuminate\Database\Seeder;

class ClipsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 10; $i++) {
            DB::table('clips')->insert([
                'name' => str_random(10),
                'start_time' => rand(0, 30),
                'end_time' => rand(31, 90),
                'created_at' => date("Y-m-d H:i:s"),
                'video_id' => 1,
            ]);
        }
    }
}
