<?php

use Illuminate\Database\Seeder;

class VideosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('videos')->insert([
            'name' => str_random(10),
            'url' => 'http://static.videogular.com/assets/videos/videogular.mp4',
            'length' => '97',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
    }
}
