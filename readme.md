# Krossover Exercise

## Local Setup

If you have virtualbox + vagrant + homestead box then you can run the
following steps to get this project running locally after cloning the
repository.

```
cd /path/to/project
composer install
php vendor/bin/homestead make
cp .env.example .env
```
Before continuing, please make sure you do not have
any other vagrant vms running using homestead.app as
the url. You can check your running vms using following
command: `vagrant global-status`

```
vagrant up
vagrant ssh
cd /path/to/project
php artisan key:generate
php artisan migrate --seed
```
Please make sure to add `192.168.10.10 homstead.app` into your `/etc/hosts` file
Then you can navigate to `http://homestead.app` and the app should load up.

For other custom local setups please run the following
```
cd /path/to/project
composer install
cp .env.example .env
```
Update all database info in `.env` file to match local settings.
Then run the following commands:
```
php artisan key:generate
php artisan migrate --seed
```
And the project should be ready for use.