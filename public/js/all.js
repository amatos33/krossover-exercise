var clipy = angular.module('clipy', [
	'ui.router',
	"ngSanitize",
	"com.2fdevs.videogular",
	"com.2fdevs.videogular.plugins.controls",
	"com.2fdevs.videogular.plugins.overlayplay",
	"com.2fdevs.videogular.plugins.poster"
]);
clipy.config(function ($interpolateProvider, $stateProvider, $urlRouterProvider, $locationProvider) {

	$interpolateProvider.startSymbol('<%');
	$interpolateProvider.endSymbol('%>');

	$stateProvider
		.state('main', {
			abstract: true,
			// Note: abstract still needs a ui-view for its children to populate.
			// You can simply add it inline here.
			templateUrl: 'partials/index',
			controller: 'MainCtrl'
		})
		.state('main.dashboard', {
			url: '/',
			views: {
				'videoContainer': {
					templateUrl: 'partials/video/index',
					controller: 'VideoCtrl'
				},
				'clipsContainer': {
					templateUrl: 'partials/clip/index',
					controller: 'ClipsCtrl'
				}
			}
		})
		.state('addVideo', {
			url: 'add-video',
			templateUrl: 'partials/video/show',
			controller: 'VideoFormCtrl'
		})
	;

	$locationProvider.html5Mode(true);
	//$urlRouterProvider.otherwise('/');

});
clipy.controller('ClipFormCtrl', ['ClipFactory', '$rootScope', function (ClipFactory, $rootScope) {

    var controller = this;

    angular.extend(controller, {
        newClip: {
            name: '',
            start_time: '',
            end_time: ''
        }
    });

    controller.addClip = function () {
        controller.newClip.video_id = $rootScope.currentVideo.id;
        ClipFactory.storeOne(controller.newClip).then(function () {
            controller.newClip = {};
            $rootScope.$broadcast('updateClipList');
        });
    };
}]);
clipy.controller('ClipsCtrl', ['$scope', 'ClipFactory', '$rootScope', 'VideoFactory', function ($scope, ClipFactory, $rootScope, VideoFactory) {

    var controller = this;

    angular.extend($scope, {
        clips: [],
        clipEditing: false,
        editedClip: {},
        oldClipInfo: {},
        oldClipIndex: null,
        currentClip: {
            start_time: 0,
            end_time: null
        }
    });

    init();

    function init() {
        getClips();
    }

    function getClips() {
        if(null !== $rootScope.currentVideo) {
            VideoFactory.getAllClips($rootScope.currentVideo).then(function (data) {
                controller.clips = data;
            });
        }
    }

    controller.deleteClip = function (clip) {

        controller.clips = $.grep(controller.clips, function(item) {
            return item != clip;
        });

        ClipFactory.deleteOne(clip.id).then(function () {
            console.log('deleted clip');
            $scope.$emit('updateClipList');
        });
    };

    controller.editClip = function (clip) {
        var index = controller.clips.indexOf(clip);
        controller.oldClipInfo = angular.copy(controller.clips[index]);
        controller.oldClipIndex = index;
        controller.editedClip = clip;
        controller.clipEditing = true;
    };

    controller.updateClip = function (clip) {
        controller.clips[controller.oldClipIndex] = clip;
        controller.clipEditing = false;

        ClipFactory.updateOne(clip).then(function (data) {
            controller.oldClipIndex = null;
            controller.oldClipInfo = {};
            console.log('Updated clip on the server:', data);
            $scope.$emit('updateClipList');
        });
    };

    controller.playClip = function (clip) {
        var frag = "#t=" + clip.start_time + ',' + clip.end_time;
        $rootScope.$broadcast('playClip', { fragment: frag });
    };

    controller.cancelEditing = function () {
        controller.clips[controller.oldClipIndex] = controller.oldClipInfo;
        controller.clipEditing = false;
        controller.editedClip = {};
    };

    $scope.$on('updateClipList', function () {
        getClips();
    });

}]);
clipy.controller('MainCtrl', ['$rootScope', function ($rootScope) {

   //TODO -- works with $rootscope but maybe there is a better
   angular.extend($rootScope, {
      currentVideo: null
   });

}]);
clipy.controller('VideoCtrl', ['$sce', 'VideoFactory', '$scope', '$timeout', '$rootScope', function ($sce, VideoFactory, $scope, $timeout, $rootScope) {
    var controller = this;

    angular.extend(controller, {
        API: null,
        videoList: [],
        currentVideo: {},
        config: {
            autoPlay: false,
            sources: [
                // {
                //     src: $sce.trustAsResourceUrl("/videos/SampleVideo.mp4"),
                //     type: "video/mp4"
                // }
            ],
            theme: {
                url: "http://www.videogular.com/styles/themes/default/latest/videogular.css"
            }
        }
    });

    init();

    controller.setCurrentVideo = function (video) {
        controller.currentVideo = video;
        $rootScope.currentVideo = video;
        $rootScope.$broadcast('updateClipList', { video: video });
        changeVideoPlayerSource(video.url);
    };

    controller.onPlayerReady = function(API) {
        controller.API = API;

        $scope.$on('playClip', function (event, args) {
            changeVideoPlayerSource(controller.currentVideo.url + args.fragment);
            clickPlay();
        });
    };

    controller.deleteVideo = function (id) {
        VideoFactory.deleteOne(id).then(function () {
            $scope.$emit('updateVideos');
        });
    };

    function init() {
        getVideos();
    }

    function getVideos() {
        VideoFactory.getAll().then(function (data) {
            controller.videoList = data;
            if(controller.videoList.length > 0) {
                controller.setCurrentVideo(controller.videoList[0]);
            }
        });
    }

    function clickPlay() {
        controller.API.stop();
        $timeout(function () {
            angular.element('.iconButton.play').triggerHandler('click');
        });
    }

    function changeVideoPlayerSource(videoUrl) {
        if(null !== controller.API) {
            controller.API.stop();
        }

        controller.config.sources = [
            {
                src: $sce.trustAsResourceUrl(videoUrl),
                type: "video/mp4"
            }
        ];
    }

    $scope.$on('updateVideos', function () {
        getVideos();
    });

}]);
clipy.controller('VideoFormCtrl', ['$scope', 'VideoFactory', '$state',
	function($scope, videosFactory, $state) {

		var controller = this;

		angular.extend(controller, {
			newVideo: {
				name: '',
				url: '',
				length: ''
			}
		});

		controller.addVideo = function() {
			videosFactory.storeOne(controller.newVideo).then(function() {
				$scope.$emit('updateVideos');
				$state.go('main.dashboard');
			});
		};
	}
]);
clipy.factory('ClipFactory', ['$http', function ($http) {
	var factory = {};
	var endpoint = '/api/clips';

	factory.getAll = function () {
		return $http.get(endpoint).then(function (response) {
			return response.data.clips;
		});
	};

	factory.deleteOne = function (id) {
		return $http.delete(endpoint + '/' + id).then(function (response) {
			console.log(response.data);
			return response.data;
		})
	};

	factory.storeOne = function (data) {
		return $http.post(endpoint, data).then(function (response) {
			return response.data;
		})
	};

	factory.updateOne = function (data) {
		return $http.patch(endpoint + '/' + data.id, data).then(function (response) {
			return response.data;
		})
	};

	return factory;
}]);
clipy.factory('VideoFactory', ['$http', function ($http) {
	var factory = {};
	var endpoint = '/api/videos';

	factory.getAll = function () {
		return $http.get(endpoint).then(function (response) {
			return response.data.videos;
		});
	};

	factory.getAllClips = function (data) {
		return $http.get(endpoint + '/' + data.id).then(function (response) {
			return response.data.video.clips;
		});
	};

	factory.deleteOne = function (id) {
		return $http.delete(endpoint + '/' + id).then(function (response) {
			return response.data;
		})
	};

	factory.storeOne = function (data) {
		return $http.post(endpoint, data).then(function (response) {
			return response.data;
		})
	};

	factory.updateOne = function (data) {
		return $http.patch(endpoint + '/' + data.id, data).then(function (response) {
			return response.data;
		})
	};

	return factory;
}]);
//# sourceMappingURL=all.js.map
