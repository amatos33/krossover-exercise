<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ elixir('css/app.css') }}">
    <base href="/">

    <script src="{{ elixir('js/app.js') }}"></script>
    <script src="{{ elixir('js/all.js') }}"></script>
</head>
<body ng-app="clipy">

<div ui-view></div>

</body>
</html>
