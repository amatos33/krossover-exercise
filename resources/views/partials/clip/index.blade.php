<div ng-controller="ClipsCtrl as controller">
    <div ng-hide="controller.clipEditing" class="clip-list">
        <h3 class="text-capitalize"><% currentVideo.name %> list of clips</h3>
        <table class="table table-striped">
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Start Time</th>
                <th>End Time</th>
                <th>Play</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            <tr ng-repeat="clip in controller.clips">
                <td><% clip.id %></td>
                <td><% clip.name %></td>
                <td><% clip.start_time %></td>
                <td><% clip.end_time %></td>
                <td>
                    <button class="btn btn-success"
                            ng-click="controller.playClip(clip)"
                    >play
                    </button>
                </td>
                <td>
                    <button class="btn btn-primary"
                            ng-click="controller.editClip(clip)"
                    >Edit
                    </button>
                </td>
                <td>
                    <button class="btn btn-danger"
                            ng-click="controller.deleteClip(clip)"
                    >Delete
                    </button>
                </td>
            </tr>
        </table>
    </div>
    <div ng-show="controller.clipEditing" class="clip-edit">
        <form>
            <div class="col-xs-12">
                <button class="btn btn-block btn-danger"
                        ng-click="controller.cancelEditing()"
                >
                    Cancel changes
                </button>
            </div>
            <div class="form-group col-xs-12">
                <label for="name">Name</label>
                <input type="text"
                       class="form-control"
                       placeholder="Name"
                       ng-model="controller.editedClip.name"
                >
            </div>
            <div class="form-group col-xs-6">
                <label for="startTime">Start Time</label>
                <input type="text"
                       class="form-control"
                       placeholder="Start Time"
                       ng-model="controller.editedClip.start_time"
                >
            </div>
            <div class="form-group col-xs-6">
                <label for="endTime">Start Time</label>
                <input type="text"
                       class="form-control"
                       placeholder="End Time"
                       ng-model="controller.editedClip.end_time"
                >
            </div>
            <div class="col-xs-12">
                <button type="button"
                        class="btn btn-primary btn-block"
                        ng-click="controller.updateClip(controller.editedClip)"
                >Save Changes</button>
            </div>
        </form>
    </div>
</div>