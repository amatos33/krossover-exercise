<h3>Video Player</h3>
<div class="container-fluid" ng-controller="VideoCtrl as controller">
    <div class="row">

        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button"
                            class="collapsed navbar-toggle"
                            data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1"
                            aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    {{--<a href="#" class="navbar-brand">Brand</a>--}}
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#"
                               class="dropdown-toggle"
                               data-toggle="dropdown"
                               role="button"
                               aria-haspopup="true"
                               aria-expanded="false">All Videos <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                {{-- Loop of all videos --}}
                                <li ng-class="{ 'active' : video.name == controller.currentVideo.name}" ng-repeat="video in controller.videoList"><a href="#" ng-click="controller.setCurrentVideo(video)"><% video.name %></a></li>
                            </ul>
                        </li>
                        <li><a ui-sref="addVideo">Add Video</a></li>
                        <li><a href="#" ng-click="controller.deleteVideo(controller.currentVideo.id)">Delete Video</a></li>
                    </ul>
                    {{--<form class="navbar-form navbar-right">--}}
                        {{--<div class="form-group">--}}
                            {{--<input class="form-control" placeholder="Search">--}}
                        {{--</div>--}}
                        {{--<button type="submit" class="btn btn-default">Submit</button>--}}
                    {{--</form>--}}
                </div>
            </div>
        </nav>

    </div>
    <div class="row">
        <div class="videogular-container col-xs-12">
            <videogular vg-player-ready="controller.onPlayerReady($API)" vg-theme="controller.config.theme.url">
                <vg-media vg-src="controller.config.sources">
                </vg-media>
                <vg-controls>
                    <vg-play-pause-button></vg-play-pause-button>
                    <vg-time-display><% currentTime | date:'mm:ss' %></vg-time-display>
                    <vg-scrub-bar>
                        <vg-scrub-bar-current-time></vg-scrub-bar-current-time>
                    </vg-scrub-bar>
                    <vg-time-display><% timeLeft | date:'mm:ss' %></vg-time-display>
                    <vg-volume>
                        <vg-mute-button></vg-mute-button>
                        <vg-volume-bar></vg-volume-bar>
                    </vg-volume>
                    <vg-fullscreen-button></vg-fullscreen-button>
                </vg-controls>
            </videogular>
        </div>
    </div>
</div>
<div class="container-fluid" ng-controller="ClipFormCtrl as controller">
    <form class="clip-form">
        <div class="form-group col-xs-12">
            <label for="name">Name</label>
            <input type="text"
                   class="form-control"
                   id="name"
                   placeholder="Name"
                   ng-model="controller.newClip.name"
            >
        </div>
        <div class="form-group col-xs-6">
            <label for="startTime">Start Time (Seconds)</label>
            <input type="text"
                   class="form-control"
                   id="startTime"
                   placeholder="Start Time"
                   ng-model="controller.newClip.start_time"
            >
        </div>
        <div class="form-group col-xs-6">
            <label for="endTime">End Time (Seconds)</label>
            <input type="text"
                   class="form-control"
                   id="endTime"
                   placeholder="End Time"
                   ng-model="controller.newClip.end_time"
            >
        </div>
        <div class="col-xs-12">
            <button type="button"
                    class="btn btn-success btn-block"
                    ng-click="controller.addClip()"
            >Add Clip</button>
        </div>
    </form>
</div>