<div class="container">
    <h1>Add Video</h1>
    <div class="col-xs-offset-3 col-xs-6" ng-controller="VideoFormCtrl as controller">
        <form>
            <div class="form-group row">
                <div class="col-xs-12">
                    <label for="name">Name</label>
                    <input type="text"
                           class="form-control"
                           id="name"
                           placeholder="Name"
                           ng-model="controller.newVideo.name"
                    >
                </div>
            </div>
            <div class="form-group row">
                <div class="col-xs-12">
                    <label for="url">Url</label>
                    <input type="text"
                           class="form-control"
                           id="url"
                           placeholder="Url"
                           ng-model="controller.newVideo.url"
                    >
                </div>
            </div>
            <div class="form-group row">
                <div class="col-xs-6">
                    <label for="length">Video Length (Seconds)</label>
                    <input type="text"
                           class="form-control"
                           id="length"
                           placeholder="Video Length (seconds)"
                           ng-model="controller.newVideo.length"
                    >
                </div>
            </div>
            <div class="form-group row">
                <div class="col-xs-6">
                    <button type="button"
                            class="btn btn-success btn-block"
                            ng-click="controller.addVideo()"
                    >Add Video</button>
                </div>
                <div class="col-xs-6">
                    <a class="btn btn-danger btn-block" ui-sref="main.dashboard">Cancel</a>
                </div>
            </div>
        </form>
    </div>
</div>