<div class="container">
    <div class="row">
        <h1>Main Dashboard</h1>
    </div>
    <div class="row">
        {{-- Video Container --}}
        <div class="col-md-7">
            <div ui-view="videoContainer"></div>
        </div>
        {{-- List Container --}}
        <div class="col-md-5">
            <div ui-view="clipsContainer"></div>
        </div>
    </div>
</div>