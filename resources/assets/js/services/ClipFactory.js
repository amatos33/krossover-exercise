clipy.factory('ClipFactory', ['$http', function ($http) {
	var factory = {};
	var endpoint = '/api/clips';

	factory.getAll = function () {
		return $http.get(endpoint).then(function (response) {
			return response.data.clips;
		});
	};

	factory.deleteOne = function (id) {
		return $http.delete(endpoint + '/' + id).then(function (response) {
			console.log(response.data);
			return response.data;
		})
	};

	factory.storeOne = function (data) {
		return $http.post(endpoint, data).then(function (response) {
			return response.data;
		})
	};

	factory.updateOne = function (data) {
		return $http.patch(endpoint + '/' + data.id, data).then(function (response) {
			return response.data;
		})
	};

	return factory;
}]);