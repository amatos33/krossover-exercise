clipy.config(function ($interpolateProvider, $stateProvider, $urlRouterProvider, $locationProvider) {

	$interpolateProvider.startSymbol('<%');
	$interpolateProvider.endSymbol('%>');

	$stateProvider
		.state('main', {
			abstract: true,
			// Note: abstract still needs a ui-view for its children to populate.
			// You can simply add it inline here.
			templateUrl: 'partials/index',
			controller: 'MainCtrl'
		})
		.state('main.dashboard', {
			url: '/',
			views: {
				'videoContainer': {
					templateUrl: 'partials/video/index',
					controller: 'VideoCtrl'
				},
				'clipsContainer': {
					templateUrl: 'partials/clip/index',
					controller: 'ClipsCtrl'
				}
			}
		})
		.state('addVideo', {
			url: 'add-video',
			templateUrl: 'partials/video/show',
			controller: 'VideoFormCtrl'
		})
	;

	$locationProvider.html5Mode(true);
	//$urlRouterProvider.otherwise('/');

});