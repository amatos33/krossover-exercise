clipy.controller('ClipsCtrl', ['$scope', 'ClipFactory', '$rootScope', 'VideoFactory', function ($scope, ClipFactory, $rootScope, VideoFactory) {

    var controller = this;

    angular.extend($scope, {
        clips: [],
        clipEditing: false,
        editedClip: {},
        oldClipInfo: {},
        oldClipIndex: null,
        currentClip: {
            start_time: 0,
            end_time: null
        }
    });

    init();

    function init() {
        getClips();
    }

    function getClips() {
        if(null !== $rootScope.currentVideo) {
            VideoFactory.getAllClips($rootScope.currentVideo).then(function (data) {
                controller.clips = data;
            });
        }
    }

    controller.deleteClip = function (clip) {

        controller.clips = $.grep(controller.clips, function(item) {
            return item != clip;
        });

        ClipFactory.deleteOne(clip.id).then(function () {
            console.log('deleted clip');
            $scope.$emit('updateClipList');
        });
    };

    controller.editClip = function (clip) {
        var index = controller.clips.indexOf(clip);
        controller.oldClipInfo = angular.copy(controller.clips[index]);
        controller.oldClipIndex = index;
        controller.editedClip = clip;
        controller.clipEditing = true;
    };

    controller.updateClip = function (clip) {
        controller.clips[controller.oldClipIndex] = clip;
        controller.clipEditing = false;

        ClipFactory.updateOne(clip).then(function (data) {
            controller.oldClipIndex = null;
            controller.oldClipInfo = {};
            console.log('Updated clip on the server:', data);
            $scope.$emit('updateClipList');
        });
    };

    controller.playClip = function (clip) {
        var frag = "#t=" + clip.start_time + ',' + clip.end_time;
        $rootScope.$broadcast('playClip', { fragment: frag });
    };

    controller.cancelEditing = function () {
        controller.clips[controller.oldClipIndex] = controller.oldClipInfo;
        controller.clipEditing = false;
        controller.editedClip = {};
    };

    $scope.$on('updateClipList', function () {
        getClips();
    });

}]);