clipy.controller('VideoFormCtrl', ['$scope', 'VideoFactory', '$state',
	function($scope, videosFactory, $state) {

		var controller = this;

		angular.extend(controller, {
			newVideo: {
				name: '',
				url: '',
				length: ''
			}
		});

		controller.addVideo = function() {
			videosFactory.storeOne(controller.newVideo).then(function() {
				$scope.$emit('updateVideos');
				$state.go('main.dashboard');
			});
		};
	}
]);