clipy.controller('VideoCtrl', ['$sce', 'VideoFactory', '$scope', '$timeout', '$rootScope', function ($sce, VideoFactory, $scope, $timeout, $rootScope) {
    var controller = this;

    angular.extend(controller, {
        API: null,
        videoList: [],
        currentVideo: {},
        config: {
            autoPlay: false,
            sources: [
                // {
                //     src: $sce.trustAsResourceUrl("/videos/SampleVideo.mp4"),
                //     type: "video/mp4"
                // }
            ],
            theme: {
                url: "http://www.videogular.com/styles/themes/default/latest/videogular.css"
            }
        }
    });

    init();

    controller.setCurrentVideo = function (video) {
        controller.currentVideo = video;
        $rootScope.currentVideo = video;
        $rootScope.$broadcast('updateClipList', { video: video });
        changeVideoPlayerSource(video.url);
    };

    controller.onPlayerReady = function(API) {
        controller.API = API;

        $scope.$on('playClip', function (event, args) {
            changeVideoPlayerSource(controller.currentVideo.url + args.fragment);
            clickPlay();
        });
    };

    controller.deleteVideo = function (id) {
        VideoFactory.deleteOne(id).then(function () {
            $scope.$emit('updateVideos');
        });
    };

    function init() {
        getVideos();
    }

    function getVideos() {
        VideoFactory.getAll().then(function (data) {
            controller.videoList = data;
            if(controller.videoList.length > 0) {
                controller.setCurrentVideo(controller.videoList[0]);
            }
        });
    }

    function clickPlay() {
        controller.API.stop();
        $timeout(function () {
            angular.element('.iconButton.play').triggerHandler('click');
        });
    }

    function changeVideoPlayerSource(videoUrl) {
        if(null !== controller.API) {
            controller.API.stop();
        }

        controller.config.sources = [
            {
                src: $sce.trustAsResourceUrl(videoUrl),
                type: "video/mp4"
            }
        ];
    }

    $scope.$on('updateVideos', function () {
        getVideos();
    });

}]);