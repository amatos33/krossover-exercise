clipy.controller('ClipFormCtrl', ['ClipFactory', '$rootScope', function (ClipFactory, $rootScope) {

    var controller = this;

    angular.extend(controller, {
        newClip: {
            name: '',
            start_time: '',
            end_time: ''
        }
    });

    controller.addClip = function () {
        controller.newClip.video_id = $rootScope.currentVideo.id;
        ClipFactory.storeOne(controller.newClip).then(function () {
            controller.newClip = {};
            $rootScope.$broadcast('updateClipList');
        });
    };
}]);