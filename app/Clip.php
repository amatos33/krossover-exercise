<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clip extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'start_time', 'end_time', 'video_id'];

    /**
     * Get the video that the clip owns.
     */
    public function video()
    {
        return $this->belongsTo('App\Video');
    }

}
