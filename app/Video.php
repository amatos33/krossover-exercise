<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = ['name', 'url', 'length'];

    /**
     * Get the clips for the video.
     */
    public function clips()
    {
        return $this->hasMany('App\Clip');
    }
}
