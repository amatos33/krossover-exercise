<?php

namespace App\Http\Controllers;

use App\Video;
use Illuminate\Http\Request;

use App\Http\Requests;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {

            $videos = Video::all();

            return response()->json(compact('videos'), 200);

        } catch (Exception $e) {

            return response()->json(['errorMessage' => $e->getMessage()], 400);

        }
    }

    /**
     * Show the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {

            $video = Video::find($id);
            $video->clips;

            return response()->json(compact('video'), 200);

        } catch (Exception $e) {

            return response()->json(['errorMessage' => $e->getMessage()], 400);

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $video = Video::create($request->all());

            return response()->json(compact('video'), 200);

        } catch (Exception $e) {

            return response()->json(['errorMessage' => $e->getMessage()], 400);

        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            Video::where(['id' => $id])
              ->update($request->all());

            return response()->json([], 200);

        } catch (Exception $e){

            return response()->json(['errorMessage' => $e->getMessage()], 400);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            Video::destroy($id);

            return response()->json([], 200);

        } catch (Exception $e){

            return response()->json(['errorMessage' => $e->getMessage()], 400);

        }
    }
}
