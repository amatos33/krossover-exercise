<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;

use App\Clip;
use Illuminate\Http\Response;

class ClipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {

            $clips = Clip::all();

            return response()->json(compact('clips'), 200);

        } catch (Exception $e) {

            return response()->json(['errorMessage' => $e->getMessage()], 400);

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {

            $clip = Clip::create($request->all());

            return response()->json(compact('clip'), 200);

        } catch (Exception $e) {

            return response()->json(['errorMessage' => $e->getMessage()], 400);

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            Clip::where(['id' => $id])
                ->update($request->all());

            return response()->json([], 200);

        } catch (Exception $e){

            return response()->json(['errorMessage' => $e->getMessage()], 400);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            Clip::destroy($id);

            return response()->json([], 200);

        } catch (Exception $e){

            return response()->json(['errorMessage' => $e->getMessage()], 400);

        }
    }
}
